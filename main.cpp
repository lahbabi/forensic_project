#include <iostream>
#include <pcap.h>
#include <fstream>
#include <stdio.h>
#include <string.h>
#include <net/ethernet.h>
#include <netinet/ip.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <netinet/ip_icmp.h>
#include <netinet/udp.h>
using namespace std;
 
 
u_char *data;
char buffer[15];
FILE *log;
FILE *ip;
FILE *dns_database;
int source;
int dest;
int numero_packet=0;
struct sniff_dns {
	/* RFC 1035, section 4.1 */
	uint16_t        query_id;
	uint16_t        codes;
	uint16_t        qdcount, ancount, nscount, arcount;
};
#define DNS_QR(dns)		((ntohs(dns->codes) & 0x8000) >> 15)
#define DNS_OPCODE(dns)	((ntohs(dns->codes) >> 11) & 0x000F)
#define DNS_RCODE(dns)	(ntohs(dns->codes) & 0x000F)
/* DNS query structure */
struct dnsquery {
  char qname[12];
  char qtype[2];
  char qclass[2];
};

/* DNS answer structure */
struct dnsanswer {
  char *name;
  char atype[2];
  char aclass[2];
  char ttl[4];
  char RdataLen[2];
  char Rdata[2];
};
bool is_dns_in_file(string dns,FILE * file);
string convert_raw_to_readable(char *read){
	  string dataStr = "";
	   for (int i = 0; i < sizeof(read); i++) {
              if ((read[i] >= 32 && read[i] <= 126) || read[i] == 10 || read[i] == 11 || read[i] == 13) {
                  dataStr += (char)read[i];
              } else {
                  dataStr += ".";
              }
          }
          return dataStr;
	  }
  string convert_data(u_char *read,int size){
	  string dataStr = "";
	   for (int i = 0; i < size; i++) {
              if ((read[i] >= 32 && read[i] <= 126) || read[i] == 10 || read[i] == 11 || read[i] == 13) {
                  dataStr += (char)read[i];
              } else {
                  dataStr += ".";
              }
          }
          return dataStr;
	  }
	bool isPartOf(char *a, char *b){
   if(strstr(b,a) != NULL){    //Strstr says does b contain a
      return true;
   } 
   return false;
}
	
	


void packetHandler(u_char *userData, const struct pcap_pkthdr* pkthdr, const u_char* packet);
bool is_in_file(char * ip_adress,FILE *file);
int main(int argc, char **argv) {
  pcap_t *descr;
  char errbuf[PCAP_ERRBUF_SIZE];
  

  // open capture file for offline processing
  if(argc<2) {
    printf("[ERROR] Missing the pcap file as argument!\nUsage: ./open.out my_file.pcap\n");
    return 1;
  }
printf("Opening data_base files \n");
  descr = pcap_open_offline(argv[1], errbuf);
  if (descr == NULL) {
      cout << "pcap_open_live() failed: " << errbuf << endl;
      return 1;
  }
  log=fopen("log.txt","w");
  if(log==NULL) 
    {	printf("Unable to create file.");
    }
  ip=fopen("database/ip.txt","r");
  if(ip==NULL) 
    {	printf("Unable to create file.");
    }
    dns_database=fopen("database/anomolous_dns","r");
  if(dns_database==NULL) 
    {	printf("Unable to create file.");
    }
  
  printf("Starting processing packets \n");

  // start packet processing loop, just like live capture
  if (pcap_loop(descr, 0, packetHandler, NULL) < 0) {
      cout << "pcap_loop() failed: " << pcap_geterr(descr);
      return 1;
  }
   fclose(log);
   fclose(ip);
  cout << "capture finished" << endl;
  return 0;
}


void packetHandler(u_char *userData, const struct pcap_pkthdr* pkthdr, const u_char* packet) {
  // this is the function called in the pcap_loop()
  numero_packet++;
  const struct ethhdr* ethernetHeader;
  const struct iphdr* ipHeader;
  const struct tcphdr* tcpHeader;
  const struct icmphdr* icmph;
  const struct udphdr* udp;
 struct sniff_dns* dns;
 struct dnsquery* dns_query;
 int size=pkthdr->len;
 int size_data;
  char sourceIp[INET_ADDRSTRLEN];
 char destIp[INET_ADDRSTRLEN];
 u_int sourcePort, destPort;
  u_char * data;
  int dataLength ;
  string dataStr = "";

  // get the Ethernet header
  
	  cout<<"processing packet :  "<<numero_packet<<endl;
      ipHeader = (struct iphdr*)(packet + sizeof(struct ethhdr));
      inet_ntop(AF_INET, &(ipHeader->saddr), sourceIp, INET_ADDRSTRLEN);
      inet_ntop(AF_INET, &(ipHeader->daddr), destIp, INET_ADDRSTRLEN);
      int iphdrlen = ipHeader->ihl * 4;
      cout<<"inspecting ip header :  "<<endl;
     data= (u_char *)(packet + iphdrlen  + sizeof(struct ethhdr));
     dataLength=size-iphdrlen  + sizeof(struct ethhdr);
      string Data=convert_data(data,dataLength);
      string username("username");
      string password("password");
      string mail("mail");
      if(Data.find(username) != std::string::npos){
		fprintf(log , " WARNING:Data of username has been reaveled in packet %d \n",numero_packet);
		  
	  }
	  if(Data.find(password) != std::string::npos){
		fprintf(log , " WARNING:password transfered in packet %d \n",numero_packet);  
	  }
	  if(Data.find(mail) != std::string::npos){
		fprintf(log , " WARNING:perhaps an email has been transfered  in packet %d\n",numero_packet);  
	  }
		  
		  
		  
      
      // check if packet is TCP
      switch (ipHeader->protocol){
      case 1:  //ICMP Protocol
		
		cout<<"inspecting icmp header \n  "<<endl;
	    icmph = (struct icmphdr*)(packet + iphdrlen  + sizeof(struct ethhdr));
		if( icmph->type == 3){
			fprintf(log , " WARNING:ICMP host unreachable in packet %d \n",numero_packet);
         
		}
            break;
         
        case 2:  //IGMP Protocol
            cout<<"inspecting igmp header \n  "<<endl;
            break;
         
        case 6:  //TCP Protocol
            cout<<"inspecting tcp header \n  "<<endl;
            break;
         
        case 17: //UDP Protocol
            cout<<"inspecting udp header \n  "<<endl;
            udp=(struct udphdr*)(packet + iphdrlen  + sizeof(struct ethhdr));
            source=ntohs(udp->source);
            dest=ntohs(udp->dest);
           if(source==53 || dest==53){
			  //DNS protocol 
			  cout<<"inspecting dns_header \n  "<<endl;
			  dns=(struct sniff_dns*)(packet + iphdrlen  + sizeof(struct ethhdr)+sizeof(struct udphdr));
			  //first attack:tunneling
			  size_data=size-(iphdrlen  + sizeof(struct ethhdr)+sizeof(struct udphdr)+sizeof(sniff_dns));
			  if(size_data>512){
				fprintf(log , " WARNING:DNS TUNNELLING  \n");  
				  
			  }
			  
			  if(DNS_QR(dns)==0){
			  dns_query=(struct dnsquery *)(packet + iphdrlen  + sizeof(struct ethhdr)+sizeof(struct udphdr)+sizeof(struct sniff_dns));
			  string domain=convert_raw_to_readable(dns_query->qname);
			  if(is_dns_in_file(domain,dns_database)){
			   fprintf(log, "this is blacklisted domain");  
		  }
			  
		  }
			  
		 }
            break;
         
        default: 
            break;
		}
          cout << sourceIp << ":" << sourcePort << " -> " << destIp << ":" << destPort << endl;
          if(is_in_file(sourceIp,ip)){
			   fprintf(log, "this is a hardcoded src adress %s  \n",sourceIp);  
		  }
		   if(is_in_file(destIp,ip)){
			   fprintf(log, "this is a hardcoded destination adress %s  \n",destIp);  
		  }
        
      }
     bool is_in_file(char * ip_adress,FILE * file){
		  string str;
		  while ( fgets ( buffer, sizeof buffer, file ) != NULL ){
			  //printf("buffer ip adress %s \n",ip_adress);
			  //printf("ip adress %s \n",buffer);
			  if(strncmp(ip_adress,buffer,sizeof ip_adress)==0){
				  //printf("entrer");
				  
				  return true;
		  }
		}
		 rewind(file);
		  return  false;
		  
	  }
	  bool is_dns_in_file(string dns,FILE * file){
		  string str;
		  while ( fgets ( buffer, sizeof buffer, file ) != NULL ){
			
			  string buff(buffer, sizeof buffer);
			  if(buff.find(dns) != std::string::npos){
				  return true;
		  }
		}
		 rewind(file);
		  return  false;
		  
	  }
		  
		  
		 
 

