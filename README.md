During this project we have written a parser that try to parse different Network  malwares inside a packet:  
The kind of malware we tried to detect are the following.  
**1.anomalous ip adress **.  
Based on the traffic that we have we try to launch an alert once we find that packet has bogus or not allocated ip adress. We have database of these adresses within the folder database.  
**2.icmp unreachable**    
We parsed icmp packets and try to launch an alert once we find that destination is unreachable.
**3.anomalous values of dns**    
We tried to see if dns requests messages contain some anomalous dns domains that are also inside our database.  
**4.dns tunneling**    
Based on dns packet length we  are able to detect some case of dns tunneling. If length of packet transcend a threehold it means that there is a tunneling  
 **5.exfiltrated informations (username,password)**    
In this case we analysed payload and we try to detect some key word like username ,password if they exist we launch an alert.This method is not working for encrypted trafic TLS.   
**information about the project**
There are some pcap tests in the folder pcap_tests all the results of malicious behavior are all writen in log.txt file.  
To compile the file lauch g++  main.cpp -lpcap  
We can test that our programm is working by ./a.out pcap_tests/dns-tunnel-iodine.pcap . All malicious traffic is written in file log.txt